package com.neev.hibernate.controller;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

import com.neev.hibernate.config.HibernateUtil;

public class ServletController extends HttpServlet {
	private static final long serialVersionUID = 5495752957386379328L;

	protected void doGet(HttpServletRequest req, HttpServletResponse res) {
		  Session session = HibernateUtil.getSessionFactory().openSession();
		    session.beginTransaction();

		    // Check database version
		    String sql = "select version()";

		    String result = (String) session.createNativeQuery(sql).getSingleResult();
		    System.out.println("The MySQL Version: "+result);

		    session.getTransaction().commit();
		    session.close();

		    
		    HibernateUtil.shutdown();
	}

}
