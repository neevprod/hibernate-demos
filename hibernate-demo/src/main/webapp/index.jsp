<html>
<header>
<title>Login</title>

	<link rel='stylesheet' href='webjars/bootstrap/3.2.0/css/bootstrap.min.css'>
   <link rel="stylesheet" href="webjars/font-awesome/4.7.0/css/font-awesome.min.css"></link>
    <link href="css/style.css" rel="stylesheet"/>
</header>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-offset-5 col-md-3">
            <div class="form-login">
            <input type="text" id="userName" class="form-control input-sm chat-input" placeholder="username" />
            </br>
            <input type="password" id="userPassword" class="form-control input-sm chat-input" placeholder="password" />
            </br>
            <div class="wrapper">
            <span class="group-btn">     
                <a href="#" class="btn btn-primary btn-md">login <i class="fa fa-sign-in"></i></a>
            </span>
            <p>This is my test! This is my test again!! Welcome back Gert!!!</p>
            </div>
            </div>
        
        </div>
    </div>
</div>
<script type="text/javascript" src="webjars/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="webjars/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
